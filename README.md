# libasymtab

libasymtab is a C library for representation, serialization and deserialization of "external symbol tables".

An external symbol table is a list of mappings between name of symbol and address of symbol.
It can be used as an alternative to native symbol tables like the ELF `symtab` section, or PDB debug info.

An external symbol table removes the limitation of name lengths and native symbol table sizes. It makes it possible to maintain a symbol table for stripped binaries with no debug info.

The format of the libasymtab symbol table is independent of the platform. Therefore, symbol resolution can be streamlined by generating only the external symbol table with a platform specific tool and reading it in uniformly.

libasymtab was made for [AutoPatch](https://gitlab.com/zatherz/autopatch). AutoPatch is a program made for modifying executables at runtime by injecting the library and all mod libraries. It works by scanning for symbols that start with `patch_` in the mod libraries, then finding a symbol with the name with `patch_` removed in the running executable, and patching its address with the mod function.

The usage of external symbol tables allows modders using AutoPatch to rename symbols, maintain symbol tables for binaries completely devoid of a native symbol table, increase performance and remove bugs (by decoupling).

libasymtab documentation is available [here](https://zatherz.eu/libasymtab/asymtab_8h.html). Use the sidebar to navigate.

# Requirements

* CMake and a C11 compiler for building the library and the unit test

* Valgrind for testing
    
    * With 32 bit libraries for 32 bit/MinGW cross compilation

* Doxygen for building docs

* MinGW and MinGW GCC for MinGW cross compilation support

* Multilib compiler and 32 bit libraries suite for 32 bit cross compilation support

# Building

Use CMake to build the library:

    mkdir build
    cd build
    cmake ..
    make

Tests will be ran automatically.

You'll be able to find `libasymtab.so` inside `libasymtab` (together with the `libsds.so` dependency).

# Cross compilation

For cross compilation, standard CMake variables should be used. An additional `TESTCMD` variable is provided to choose how to run the resulting executable.

Note that Valgrind is fairly buggy with Wine and is not recommended when cross compiling.

## Examples

Host: 64 bit Linux

### Target: 32 bit Windows

	mkdir build
	cd build
	cmake \
	  -DCMAKE_SYSTEM_NAME=Windows \
	  -DCMAKE_C_COMPILER=i686-w64-mingw32-gcc \
	  -DCMAKE_CXX_COMPILER=i686-w64-mingw32-g++ \
	  -DTESTCMD="env WINEPATH='.\\libasymtab' wine" \
	  ..
	make

### Target: 64 bit Windows

   	mkdir build
	cd build
	cmake \
	  -DCMAKE_SYSTEM_NAME=Windows \
	  -DCMAKE_C_COMPILER=x86_64-w64-mingw32-gcc \
	  -DCMAKE_CXX_COMPILER=x86_64-w64-mingw32-g++ \
	  -DTESTCMD="env WINEPATH='.\\libasymtab' wine" \
	  ..
	make

### 32 bit Linux on 64 bit host

To compile for 32 bits, just pass the `-m32` option:

	mkdir build
	cd build
	cmake -DCMAKE_C_FLAGS="-m32" -DCMAKE_CXX_FLAGS="-m32" ..
	make

# Building docs

    doxygen