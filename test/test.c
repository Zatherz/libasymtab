#include <stdio.h>
#include <asymtab.h>
#include <string.h>
#include <sds/sds.h>
#include <sds/testhelp.h>
#include <stdbool.h>
#include <time.h>

static char* random_ascii(size_t len) {
  static const char* alphanum_lookup =
          "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
          "abcdefghijklmnopqrstuvwxyz"
          "0123456789";
  static const int alphanum_lookup_size = 62;

  char* str = malloc(sizeof(char) * len + 1);
  for (size_t i = 0; i < len; i++) {
    str[i] = alphanum_lookup[rand() % (alphanum_lookup_size - 1)];
  }
  str[len] = '\0';

  return str;
}

int main(int argc, char** argv) {
  asymtab_error_t err;

  {
    puts("=== SYMBOL CREATION ===");
    asymbol_t* sym = asymbol_new("main", 71823, "Program entry point", &err);
    test_cond("was allocated", sym != NULL);
    test_cond("name is intact", strcmp(sym->name, "main") == 0);
    test_cond("address is intact", sym->address == 71823);
    test_cond("description is intact", strcmp(sym->description, "Program entry point") == 0);

    asymbol_add_flag(sym, "test1", &err);
    asymbol_add_flag(sym, "test2", &err);
    test_cond("flags_len == 2", sym->flags_len == 2);

    puts("\n=== SYMBOL SERIALIZATION ===");
    sds ser = asymbol_serialize(sym);
    // main[NUL]71823[NUL]Program entry point[NUL]
    // 31 characters

    test_cond("was serialized", ser != NULL);
    test_cond("has correct length", sdslen(ser) == 44);

    sds* split_ary;
    int split_len;

    split_ary = sdssplitlen(ser, sdslen(ser), "\0", 1, &split_len);

    test_cond("split into 5 elements", split_len == 5);
    test_cond("name was serialized properly", strcmp(split_ary[0], "main") == 0);
    test_cond("address was serialized properly", strcmp(split_ary[1], "71823") == 0);
    test_cond("flags were serialized properly", strcmp(split_ary[2], "test1;test2") == 0);
    test_cond("description was serialized properly", strcmp(split_ary[3], "Program entry point") == 0);
    test_cond("last split element is just a newline", strcmp(split_ary[4], "\n") == 0);

    sdsfreesplitres(split_ary, split_len);
    asymbol_free(sym);

    puts("\n=== SYMBOL DESERIALIZATION ===");
    sym = asymbol_deserialize(ser, &err); // reusing 'sym'
    test_cond("was deserialized", sym != NULL);
    test_cond("name is intact", strcmp(sym->name, "main") == 0);
    test_cond("address is intact", sym->address == 71823);
    test_cond("flags are intact", sym->flags_len == 2 && strcmp(sym->flags[0], "test1") == 0 && strcmp(sym->flags[1], "test2") == 0);
    test_cond("description is intact", strcmp(sym->description, "Program entry point") == 0);

    puts("\n=== SYMBOL RESERIALIZATION ===");
    sds ser2 = asymbol_serialize(sym);
    test_cond("same as previous serialization output", sdscmp(ser, ser2) == 0);

    sdsfree(ser);
    sdsfree(ser2);
    asymbol_free(sym);
  }

  {
    puts("\n=== SYMBOL TABLE CREATION ===");
    asymtab_t* symtab = asymtab_new_generator("test", "Unit tests for libasymtab", &err);
    test_cond("was allocated", symtab != NULL);
    test_cond("0 symbols", symtab->symbols_len == 0);
    test_cond("trying to get a symbol by index results in NULL", asymtab_symbol_at(symtab, 42) == NULL);
    test_cond("trying to get a symbol by name results in NULL", asymtab_symbol_by_name(symtab, "test") == NULL);

    puts("\n=== ADDING TO SYMBOL TABLE ===");
    {
      asymbol_t* sym = asymbol_new("main", 182374, "Entry point", &err);
      asymtab_add(symtab, sym, &err);
      test_cond("1 symbol in table", symtab->symbols_len == 1);
      test_cond("trying to get a symbol with index 1 results in NULL", asymtab_symbol_at(symtab, 1) == NULL);
      test_cond("trying to get a symbol with random name results in NULL", asymtab_symbol_by_name(symtab, "test") == NULL);
      test_cond("symbol at index 0 isn't NULL", asymtab_symbol_at(symtab, 0) != NULL);
      test_cond("symbol at index 0 has name 'main'", strcmp(asymtab_symbol_at(symtab, 0)->name, "main") == 0);
      test_cond("symbol with name 'main' has description 'Entry point'", strcmp(asymtab_symbol_by_name(symtab, "main")->description, "Entry point") == 0);

      puts("\n=== ADDING ENOUGH SYMBOLS TO SYMBOL TABLE TO TRIGGER 4 ALLOCATIONS ===");
      // force 4 allocations
      srand(time(NULL));
      for (int i = 0; i < ASYMTAB_MIN_CAPACITY_PER_GROW * 4; i++) {
        char* name = random_ascii(5);
        char* desc = random_ascii(20);

        asymtab_add(symtab, asymbol_new(name, rand(), desc, &err), &err);

        free(name);
        free(desc);
      }

      {
        size_t expected_size = 1 + ASYMTAB_MIN_CAPACITY_PER_GROW * 4;
        sds msg = sdsfromlonglong((long long)expected_size);
        msg = sdscat(msg, " symbols in table");

        test_cond(msg, symtab->symbols_len == expected_size);

        sdsfree(msg);
      }

      puts("\n=== REMOVING SYMBOL FROM SYMBOL TABLE ===");

      size_t prev_len = symtab->symbols_len;
      {
        bool  result = asymtab_remove(symtab, sym);
        test_cond("successfully removed symbol", result == true);
        {
          sds msg = sdsfromlonglong((long long)(prev_len - 1));
          msg = sdscat(msg, " symbols in table");

          test_cond(msg, prev_len - 1)

          sdsfree(msg);
        }
        test_cond("asymtab_remove_free doesn't free if symbol doesn't exist in the table", asymtab_remove_free(symtab, sym) != true);
        asymtab_add(symtab, sym, &err);
      }
      {
        bool result = asymtab_remove_free(symtab, sym);
        test_cond("asymtab_remove_free removes and frees if the symbol does exist in the table", result && symtab->symbols_len == (prev_len - 1));
      }

      char* test_file_path = "serialization_test.dat";
      if (argc >= 2) test_file_path = argv[1];

      {
        puts("\n=== SYMBOL TABLE SERIALIZATION TO FILE ===");
        sds serialized = asymtab_serialize(symtab);
        test_cond("successfully serialized", serialized != NULL);

        FILE* ser_f = fopen(test_file_path, "wb");
        test_cond("file opened for writing", ser_f != NULL);
        fwrite(serialized, 1, sdslen(serialized), ser_f);
        fclose(ser_f);

        sdsfree(serialized);
      }

      asymtab_t* symtab2;
      {
        puts("\n=== SYMBOL TABLE RESERIALIZATION FROM FILE (using asymtab_deserialize) ===");
        FILE* deser_f = fopen(test_file_path, "rb");
        test_cond("file opened for reading", deser_f != NULL);
        fseek(deser_f, 0, SEEK_END);
        long deser_size = ftell(deser_f);
        rewind(deser_f);

        char* deser_buf = malloc(sizeof(char) * (deser_size + 1));
        fread(deser_buf, deser_size, 1, deser_f);
        fclose(deser_f);

        sds serialized = sdsnewlen(deser_buf, deser_size);
        symtab2 = asymtab_deserialize(serialized, &err);
        test_cond("successfully deserialized", symtab2 != NULL);
        test_cond("size intact", symtab2->symbols_len == symtab->symbols_len);
        test_cond("generator id intact ('test')", strcmp(symtab2->generator_id, "test") == 0);
        test_cond("generator name intact ('Unit tests for libasymtab')", strcmp(symtab2->generator_name, "Unit tests for libasymtab") == 0);
        test_cond("different addresses of first element", symtab2->symbols[0] != symtab->symbols[0]);

        sdsfree(serialized);
        free(deser_buf);
      }

      {
        puts("\n=== SYMBOL TABLE RESERIALIZATION FROM FILE (using asymtab_read) ===");
        asymtab_t* symtab3 = asymtab_read(test_file_path, &err);
        test_cond("successfully deserialized", symtab3 != NULL);
        test_cond("size intact", symtab3->symbols_len == symtab->symbols_len);
        test_cond("generator id intact ('test')", strcmp(symtab3->generator_id, "test") == 0);
        test_cond("generator name intact ('Unit tests for libasymtab')", strcmp(symtab3->generator_name, "Unit tests for libasymtab") == 0);
        test_cond("different addresses of first element", symtab3->symbols[0] != symtab->symbols[0] && symtab3->symbols[0] != symtab2->symbols[0]);

        asymtab_free_with_symbols(symtab2);
        asymtab_free_with_symbols(symtab3);
      }
      
      remove(test_file_path);
    }

    asymtab_free_with_symbols(symtab);
  }

  {
    puts("\n=== ERROR HANDLING ===");

    asymtab_t* symtab = asymtab_read("/////\\\\\this path doesn't exist blah blah blah...\\\\/////", &err);
    test_cond("returned NULL for file that doesn't exist", symtab == NULL);
    test_cond("error is ASYMTAB_ERROR_FILE_OPEN_FAIL", err == ASYMTAB_ERROR_FILE_OPEN_FAIL);
    test_cond("error message is 'Could not open file'", strcmp(asymtab_error_message(err), "Could not open file") == 0);
  }

  puts("\n=== FINISHED ===");
  test_report();
}
