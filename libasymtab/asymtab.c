//! @cond
#define ASYMTAB_SOURCE
//! @endcond
#include "asymtab.h"
//! @cond
#undef ASYMTAB_SOURCE
//! @endcond
#include <stdbool.h>
#include <assert.h>
#include "sds/sds.h"

asymtab_t* asymtab_new_capacity(size_t initial_capacity, char* generator_id, char* generator_name, asymtab_error_t* err) {
  if (err != NULL) *err = ASYMTAB_OK;

  asymtab_t* tab = malloc(sizeof(asymtab_t));
  if (tab == NULL) {
    if (err != NULL) *err = ASYMTAB_ERROR_ALLOC_FAIL;
    return NULL;
  }

  tab->_symbols_capacity = initial_capacity;
  tab->symbols = malloc(sizeof(asymbol_t*) * initial_capacity);
  tab->symbols_len = 0;

  char* generator_id_cpy = NULL;
  if (generator_id != NULL) {
    generator_id_cpy = malloc(sizeof(char) * (strlen(generator_id) + 1));
    strcpy(generator_id_cpy, generator_id);
    generator_id_cpy[strlen(generator_id)] = '\0';
  }
  tab->generator_id = generator_id_cpy;

  char* generator_name_cpy = NULL;
  if (generator_name != NULL) {
    generator_name_cpy = malloc(sizeof(char) * (strlen(generator_name) + 1));
    strcpy(generator_name_cpy, generator_name);
    generator_name_cpy[strlen(generator_name)] = '\0';
  }
  tab->generator_name = generator_name_cpy;
  return tab;
}

asymtab_t* asymtab_new_generator(char* generator_id, char* generator_name, asymtab_error_t* err) {
  return asymtab_new_capacity(ASYMTAB_MIN_CAPACITY_PER_GROW, generator_id, generator_name, err);
}

asymtab_t* asymtab_new(asymtab_error_t* err) {
  return asymtab_new_generator(NULL, NULL, err);
}

static void grow_asymtab(asymtab_t* symtab, size_t element_count, asymtab_error_t* err) {
  if (err != NULL) *err = ASYMTAB_OK;
  if (symtab->_symbols_capacity >= symtab->symbols_len + element_count) return;

  size_t new_capacity = symtab->_symbols_capacity + MAX(ASYMTAB_MIN_CAPACITY_PER_GROW, element_count);
  asymbol_t** new_sym_pointer = realloc(symtab->symbols, sizeof(struct asymbol_t*) * new_capacity);

  if (new_sym_pointer == NULL) {
    if (err != NULL) *err = ASYMTAB_ERROR_REALLOC_FAIL;
    return;
  }

  symtab->symbols = new_sym_pointer;
  symtab->_symbols_capacity = new_capacity;
}

void asymtab_add(asymtab_t* symtab, asymbol_t* symbol, asymtab_error_t* err) {
  grow_asymtab(symtab, 1, err);

  symtab->symbols[symtab->symbols_len] = symbol;
  symtab->symbols_len += 1;
}

asymbol_t* asymtab_symbol_at(asymtab_t* symtab, size_t index) {
  if (symtab->symbols_len <= index) return NULL;
  return symtab->symbols[index];
}

asymbol_t* asymtab_symbol_by_name(asymtab_t* symtab, char* name) {
  for (size_t i = 0; i < symtab->symbols_len; i++) {
    asymbol_t* sym = symtab->symbols[i];
    if (strcmp(sym->name, name) == 0) return sym;
  }
  return NULL;
}

void asymtab_free_with_symbols(asymtab_t* symtab) {
  for (size_t i = 0; i < symtab->symbols_len; i++) {
    asymbol_free(symtab->symbols[i]);
  }
  if (symtab->generator_id != NULL) free(symtab->generator_id);
  if (symtab->generator_name != NULL) free(symtab->generator_name);
  asymtab_free(symtab);
}

void asymtab_free(asymtab_t* symtab) {
  free(symtab->symbols);
  free(symtab);
}

sds asymtab_serialize(asymtab_t* symtab) {
  sds serialized = sdsempty();

  size_t first_line_len = strlen(ASYMTAB_FORMAT_VERSION) + 1 + strlen(ASYMTAB_FORMAT_FEATURES) + 1 + 1 + 2;
  size_t generator_id_len = 0;
  size_t generator_name_len = 0;
  if (symtab->generator_id != NULL) {
    first_line_len += (generator_id_len = strlen(symtab->generator_id));
  }
  if (symtab->generator_name != NULL) {
    first_line_len += (generator_name_len = strlen(symtab->generator_name));
  }

  // version + 1 + features + 1 + generatorid + 1 + generatorname + 2
  serialized = sdscatlen(serialized, ASYMTAB_FORMAT_VERSION "\0" ASYMTAB_FORMAT_FEATURES "\0", strlen(ASYMTAB_FORMAT_VERSION) + 1 + strlen(ASYMTAB_FORMAT_FEATURES) + 1);
  serialized = sdscatlen(serialized, symtab->generator_id, generator_id_len);
  serialized = sdscatlen(serialized, "\0", 1);
  serialized = sdscatlen(serialized, symtab->generator_name, generator_name_len);
  serialized = sdscatlen(serialized, "\0\n", 2);

  for (int i = 0; i < symtab->symbols_len; i++) {
    sds sym_serialized = asymbol_serialize(symtab->symbols[i]);
    serialized = sdscatsds(serialized, sym_serialized);
    sdsfree(sym_serialized);
  }

  return serialized;
}

asymtab_t* asymtab_deserialize(sds str, asymtab_error_t* err) {
  if (err != NULL) *err = ASYMTAB_OK;
  
  int split_len;
  // symbols are always terminated with a null byte followed by a newline
  sds* split_ary = sdssplitlen(str, sdslen(str), "\0\n", 2, &split_len);

  asymtab_t* symtab = asymtab_new(err);

  // first element is metadata about symbol table
  sds first = split_ary[0];
  int meta_split_len;
  sds* meta_split_ary = sdssplitlen(first, sdslen(first), "\0", 1, &meta_split_len);

  if (meta_split_len < 4) {
    if (err != NULL) *err = ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_VERSION;
    return NULL;
  }
  sds format_version = meta_split_ary[0];
  sds format_features = meta_split_ary[1];
  sds generator_id = meta_split_ary[2];
  sds generator_name = meta_split_ary[3];

  if (strcmp(format_version, ASYMTAB_FORMAT_VERSION) != 0) {
    if (err != NULL) *err = ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_VERSION;
    return NULL;
  }
  if (strcmp(format_features, ASYMTAB_FORMAT_FEATURES) != 0) {
    if (err != NULL) *err = ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_FEATURES;
    return NULL;
  }

  char* generator_id_cpy = NULL; 
  char* generator_name_cpy = NULL;

  if (sdslen(generator_id) > 0) {
    generator_id_cpy = malloc(sizeof(char) * (sdslen(generator_id) + 1));
    strcpy(generator_id_cpy, generator_id);
    generator_id_cpy[sdslen(generator_id)] = '\0';
  }

  if (sdslen(generator_name) > 0) {
    generator_name_cpy = malloc(sizeof(char) * (sdslen(generator_name) + 1));
    strcpy(generator_name_cpy, generator_name);
    generator_name_cpy[sdslen(generator_name)] = '\0';
  }

  symtab->generator_id = generator_id_cpy;
  symtab->generator_name = generator_name_cpy;

  sdsfreesplitres(meta_split_ary, meta_split_len);

  // last element is an empty string
  for (int i = 1; i < (split_len - 1); i++) {
    sds elem = split_ary[i];
    asymbol_t* deserialized_sym = asymbol_deserialize(elem, err);

    if (deserialized_sym == NULL) return NULL;

    asymtab_add(symtab, deserialized_sym, err);
  }

  sdsfreesplitres(split_ary, split_len);

  return symtab;
}

asymtab_t* asymtab_read(char* path, asymtab_error_t* err) {
  if (err != NULL) *err = ASYMTAB_OK;

  // TODO read line by line?
  FILE *f = fopen(path, "rb");
  if (f == NULL) {
    if (err != NULL) *err = ASYMTAB_ERROR_FILE_OPEN_FAIL;
    return NULL;
  }

  fseek(f, 0, SEEK_END);
  long size = ftell(f);
  rewind(f);

  sds content = sdsnewlen(NULL, size);
  fread(content, size, 1, f);
  fclose(f);

  asymtab_t* symtab = asymtab_deserialize(content, err);
  sdsfree(content);
  return symtab;
}

bool asymtab_remove(asymtab_t* symtab, asymbol_t* sym) {
  size_t idx = 0;
  bool found = false;
  for (size_t i = 0; i < symtab->symbols_len; i++) {
    asymbol_t* el = symtab->symbols[i];
    if (el == sym) {
      found = true;
      idx = i;
      break;
    }
  }

  if (!found) return false;

  // shift left
  for (size_t i = idx; i < symtab->symbols_len; i++) {
    if (i == (symtab->symbols_len - 1)) {
      symtab->symbols[i] = NULL;
    } else {
      symtab->symbols[i] = symtab->symbols[i + 1];
    }
  }

  symtab->symbols_len --;
  return true;
}

bool asymtab_remove_free(asymtab_t* symtab, asymbol_t* sym) {
  bool result = asymtab_remove(symtab, sym);
  if (!result) return false;
  asymbol_free(sym);
  return true;
}

#undef MAX
