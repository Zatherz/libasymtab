#include "asymtab.h"

const char* asymtab_error_message(asymtab_error_t err) {
	switch(err) {
	case ASYMTAB_OK: return "OK";
	case ASYMTAB_ERROR_ALLOC_FAIL: return "Allocation failed";
	case ASYMTAB_ERROR_FILE_OPEN_FAIL: return "Could not open file";
	case ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_VERSION: return "The symbol table is using an incompatible (most likely outdated) version of the format";
	case ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_FEATURES: return "The symbol table is using a format with incompatible features";
	case ASYMTAB_ERROR_INVALID_ADDRESS: return "Invalid address";
	default: return "Unknown error";
	}
}