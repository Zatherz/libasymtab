//! @cond
#define ASYMTAB_SOURCE
//! @endcond
#include "asymtab.h"
//! @cond
#undef ASYMTAB_SOURCE
//! @endcond
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "sds/sds.h"
#include <assert.h>

static asymbol_t* asymbol_new_flags(char* name, size_t address, char* description, char** flags, size_t flags_len, size_t flags_capacity, asymtab_error_t* err) {
  if (err != NULL) *err = ASYMTAB_OK;

  asymbol_t* sym = malloc(sizeof(struct asymbol_t));
  if (sym == NULL) {
    if (err != NULL) *err = ASYMTAB_ERROR_ALLOC_FAIL;
    return NULL;
  }

  char* m_name = NULL;
  if (name == NULL) {
    m_name = malloc(sizeof(char));
    *m_name = '\0';
  } else {
    m_name = malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(m_name, name);
  }

  char* m_desc = NULL;
  if (description == NULL) {
    m_desc = malloc(sizeof(char));
    *m_desc = '\0';
  } else {
    m_desc = malloc(sizeof(char) * (strlen(description) + 1));
    strcpy(m_desc, description);
  }

  sym->name = m_name;
  sym->address = address;
  sym->description = m_desc;

  sym->flags = flags;
  sym->flags_len = flags_len;
  sym->_flags_capacity = flags_capacity;
  
  return sym;
}

asymbol_t* asymbol_new(char* name, size_t address, char* description, asymtab_error_t* err) {
  return asymbol_new_flags(name, address, description, NULL, 0, 0, err); // save memory by not allocating flags unless needed
}

static void grow_asymbol_flags(asymbol_t* sym, size_t element_count, asymtab_error_t* err) {
  if (err != NULL) *err = ASYMTAB_OK;

  if (sym->flags == NULL) {
    size_t capacity = ASYMTAB_MIN_CAPACITY_PER_GROW;
    if (element_count > capacity) capacity = element_count;

    sym->_flags_capacity = capacity;
    sym->flags = malloc(sizeof(char*) * capacity);
    sym->flags_len = 0;

    return;
  }

  if (sym->_flags_capacity >= sym->flags_len + element_count) return;

  size_t new_capacity = sym->_flags_capacity + MAX(ASYMTAB_MIN_CAPACITY_PER_GROW, element_count);

  char** new_flags_pointer = realloc(sym->flags, sizeof(char*) * new_capacity);

  if (new_flags_pointer == NULL) {
    if (err != NULL) *err = ASYMTAB_ERROR_REALLOC_FAIL;
    return;
  }

  sym->flags = new_flags_pointer;
  sym->_flags_capacity = new_capacity;
}

void asymbol_add_flag(asymbol_t* sym, char* flag, asymtab_error_t* err) {
  grow_asymbol_flags(sym, 1, err);

  size_t len = strlen(flag);
  char* cpy = malloc(sizeof(char) * (len + 1));
  strncpy(cpy, flag, len);
  cpy[len] = '\0';

  sym->flags[sym->flags_len] = cpy;
  sym->flags_len += 1;
}

void asymbol_free(asymbol_t* sym) {
  free(sym->name);
  free(sym->description);
  if (sym->flags != NULL) {
    for (size_t i = 0; i < sym->flags_len; i++) {
      free(sym->flags[i]);
    }
    free(sym->flags);
  }
  free(sym);
}

sds asymbol_serialize(asymbol_t* sym) {
  sds serialized = sdsempty();
  serialized = sdscat(serialized, sym->name);
  serialized = sdscatlen(serialized, "\0", 1);
  sds addr_str = sdsfromlonglong((long long)sym->address);
  serialized = sdscatsds(serialized, addr_str);
  serialized = sdscatlen(serialized, "\0", 1);
  if (sym->flags != NULL) {
    for (size_t i = 0; i < sym->flags_len; i++) {
      char* flag = sym->flags[i];
      serialized = sdscat(serialized, flag);
      if (i != sym->flags_len - 1) serialized = sdscatlen(serialized, ";", 1);
    }
  }
  serialized = sdscatlen(serialized, "\0", 1);
  if (sym->description != NULL) serialized = sdscat(serialized, sym->description);
  serialized = sdscatlen(serialized, "\0\n", 2);
  sdsfree(addr_str);

  return serialized;
}

asymbol_t* asymbol_deserialize(sds str, asymtab_error_t* err) {
  if (err != NULL) *err = ASYMTAB_OK;
  int split_len; // why does sds use ints for this?...
  sds* split_ary = sdssplitlen(str, sdslen(str), "\0", 1, &split_len);

  // for performance, we ignore the last element (the newline)
  // this is so that asymtable_deserialize can pass in a string in this format:
  // name[NUL]addr[NUL]flag1;flag2[NUL]desc
  // instead of the proper
  // name[NUL]addr[NUL]flag1;flag2[NUL]desc[NUL][NEWLINE]
  // to avoid more allocations

  if (split_len < 4) {
    if (err != NULL) *err = ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_VERSION;
    return NULL;
  }

  sds name = split_ary[0];
  sds addr_str = split_ary[1];
  sds flags_str = split_ary[2];
  sds desc = split_ary[3];
  if (strlen(desc) == 0) desc = NULL;
  if (strlen(flags_str) == 0) flags_str = NULL;

  size_t addr;
  int result = sscanf(addr_str, SIZE_T_FORMAT, &addr);
  if (result < 1) {
    if (err != NULL) *err = ASYMTAB_ERROR_INVALID_ADDRESS;
    return NULL;
  }

  char** flags;
  size_t flags_len;
  size_t flags_capacity;

  if (flags_str == NULL) {
    flags = NULL;
    flags_len = 0;
    flags_capacity = 0;
  } else {
    int flags_split_len;
    sds* flags_split_ary = sdssplitlen(flags_str, sdslen(flags_str), ";", 1, &flags_split_len);

    flags = malloc(sizeof(char*) * flags_split_len);
    flags_len = flags_split_len;
    flags_capacity = MAX(ASYMTAB_MIN_CAPACITY_PER_GROW, flags_split_len);

    for (int i = 0; i < flags_split_len; i++) {
      sds flag = flags_split_ary[i];
      size_t flag_len = sdslen(flag);
      char* cpy = malloc(sizeof(char) * (flag_len + 1));
      strncpy(cpy, flag, flag_len);
      cpy[flag_len] = '\0';

      flags[i] = cpy; 
    }

    // all strings are copied at this point so we can safely do this
    sdsfreesplitres(flags_split_ary, flags_split_len);
  }

  asymbol_t* sym = asymbol_new_flags(name, addr, desc, flags, flags_len, flags_capacity, err);

  sdsfreesplitres(split_ary, split_len);

  return sym;
}
