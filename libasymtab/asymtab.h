#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "sds/sds.h"

/**
@file
**/

#ifndef H_ASYMTAB
#define H_ASYMTAB

//! @cond
#define ASYMTAB_MIN_CAPACITY_PER_GROW 20

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__TOS_WIN__) || defined(__WINDOWS__)
#define SIZE_T_FORMAT "%Iu" // fucking microsoft
#ifdef ASYMTAB_SOURCE
#define ASYMTAB_API __declspec(dllexport)
#else
#define ASYMTAB_API __declspec(dllimport)
#endif
#else
#define SIZE_T_FORMAT "%zu"
#define ASYMTAB_API
#endif
//! @endcond

//! @cond
#define MAX(a, b) (((a) < (b)) ? (a) : (b))
//! @endcond


/**
@brief Error enum.

Use asymtab_error_message(asymtab_error_t err) to obtain a human readable message.
**/
typedef enum asymtab_error {
  ASYMTAB_OK,
  ASYMTAB_ERROR_ALLOC_FAIL,
  ASYMTAB_ERROR_REALLOC_FAIL,
  ASYMTAB_ERROR_FILE_OPEN_FAIL,
  ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_VERSION,
  ASYMTAB_ERROR_INCOMPATIBLE_FORMAT_FEATURES,
  ASYMTAB_ERROR_INVALID_ADDRESS
} asymtab_error_t;

/**
@brief Obtain a human-readable message for a particular error.
**/
ASYMTAB_API const char* asymtab_error_message(asymtab_error_t err);

/**
@brief Symbol representation.

Use asymbol_new(char*, size_t, char*) to allocate a pointer to this struct.

Use asymbol_free(asymbol_t*) to destroy the pointer.
**/
typedef struct asymbol_t {
  //! @brief Name of the symbol.
  char* name;
  //! @brief Stored address to it (not an actually usable pointer).
  size_t address;
  //! @brief Description of the symbol.
  char* description;
  //! @brief Implementation defined flags about the symbol. Order is kept, duplicates are allowed.
  char** flags;
  //! @brief Length of the flags array (the actually used portion - memory is allocated in chunks).
  size_t flags_len;
  //! @cond
  size_t _flags_capacity;
  //! @endcond
} asymbol_t;

#define ASYMTAB_FORMAT_VERSION "4"
#define ASYMTAB_FORMAT_FEATURES "name+address+flags+description"

/**
@brief Create a new symbol object with the specified name, address and description.
@param name The string is copied to another buffer (therefore, you can `free` the pointer passed to argument after calling `symbol_new` without worries).
@param address Stored address.
@param description Can be `NULL`. Copied like `name`.
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.
  
Calling `asymbol_free` on the symbol will free both the copy of `name` and `description`.

@return a symbol on success (`err` will be set to `ASYMTAB_OK`)
@return NULL on failure (`err` will be set to the appropriate error code)
**/
ASYMTAB_API asymbol_t* asymbol_new(char* name, size_t address, char* description, asymtab_error_t* err);

/**
@brief Add an implementation defined flag string to the symbol.
@param flag The string is copied to another buffer, which means that you can `free` the passed pointer without worrying about the pointer in the symbol object.
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.
**/
ASYMTAB_API void asymbol_add_flag(asymbol_t* sym, char* flag, asymtab_error_t* err);

/**
@brief Free the symbol object, its name and description.

@note The symbol object is unusable after using this function.
**/
ASYMTAB_API void asymbol_free(asymbol_t* sym);

/**
@brief Serialize the symbol object into a string.

@note The format may change between versions, but it is guaranteed to always be compatible with asymbol_deserialize(sds).
@note The current format is: `NAME\0ADDRESS\0FLAGS\0DESCRIPTION\0\n`.

@return a @dynamicstring
**/
ASYMTAB_API sds asymbol_serialize(asymbol_t* sym);

/**
@brief Deserialize a string into a symbol object.
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.

The input is expected to have previously been created by asymbol_serialize(asymbol_t*).

@return a pointer to an asymbol_t (`err` will be set to `ASYMTAB_OK`)
@return `NULL` when parsing fails (`err` will be set to the appropriate error code)
**/
ASYMTAB_API asymbol_t* asymbol_deserialize(sds str, asymtab_error_t* err);

/**
@brief Symbol table representation.

Use asymtab_new(char*) to allocate a pointer to this struct.

Use asymtab_free(asymtab_t*) or asymtab_free_with_symbols(asymtab_t*) to destroy it.
**/
typedef struct asymtab_t {
  //! @brief Platform name in string form.
  char* platform;
  //! @brief Array of symbol objects.
  asymbol_t** symbols;
  //! @brief Length of the array (the actually used portion - memory is allocated in chunks).
  size_t symbols_len;
  //! @brief Generator ID.
  char* generator_id;
  //! @brief Human readable generator name.
  char* generator_name;
  //! @cond
  size_t _symbols_capacity;
  //! @endcond
} asymtab_t;

/**
@brief Create a new symbol table.
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.

@return a symbol table on success (`err` will be set to `ASYMTAB_OK`)
@return `NULL` on failure (`err` will be set to the appropriate error code)
**/
ASYMTAB_API asymtab_t* asymtab_new(asymtab_error_t* err);


/**
@brief Create a new symbol table.
@param generator_id ID of the generator of this symtab (will be copied).
@param generator_name Human readable name of the generator of this symtab (will be copied).
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.

@return a symbol table on success (`err` will be set to `ASYMTAB_OK`)
@return `NULL` on failure (`err` will be set to the appropriate error code)
**/
ASYMTAB_API asymtab_t* asymtab_new_generator(char* generator_id, char* generator_name, asymtab_error_t* err);

/**
@brief Create a new symbol table.
@param initial_capacity Amount of bytes to allocate as initial capacity.
@param generator_id ID of the generator of this symtab (will be copied).
@param generator_name Human readable name of the generator of this symtab (will be copied).
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.

@return a symbol table on success (`err` will be set to `ASYMTAB_OK`)
@return `NULL` on failure (`err` will be set to the appropriate error code)
**/
ASYMTAB_API asymtab_t* asymtab_new_capacity(size_t initial_capacity, char* generator_id, char* generator_name, asymtab_error_t* err);

/**
@brief Add a symbol to the symbol table.
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.
**/
ASYMTAB_API void asymtab_add(asymtab_t* symtab, asymbol_t* symbol, asymtab_error_t* err);

/**
@brief Safely get a symbol at an index.

@param index Index into the `symbols` array.

@return the symbol at the specified `index`
@return `NULL` if index is out of bounds.
**/
ASYMTAB_API asymbol_t* asymtab_symbol_at(asymtab_t* symtab, size_t index);

/**
@brief Safely get a symbol with a name.

@note Strings are compared using `strcmp`.
@note This is an O(n) operation.

@return the symbol with the passed `name`
@return `NULL` if no symbol in the symbol table has that name
**/
ASYMTAB_API asymbol_t* asymtab_symbol_by_name(asymtab_t* symtab, char* name);

/**
@brief Free all symbol objects in the symbol table, and then call asymtab_free(asymtab_t*).

@note The symbol table object is unusable after calling this function.
**/
ASYMTAB_API void asymtab_free_with_symbols(asymtab_t* symtab);

/**
@brief Free the symbol table, the platform string and the symbol array.

@note This does not free the contents of the symbol array.
@note The user is responsible for freeing any symbol that used to be in this symbol table.
@note Use asymtab_free_with_symbols(asymtab_t*) to also call asymbol_free(asymbol_t*) on all symbols in the table.
@note The symbol table object is unusable after calling this function.
**/
ASYMTAB_API void asymtab_free(asymtab_t* symtab);

/**
@brief Serialize the symbol table to a string.

@note The format may change between versions, but it is guaranteed to always be compatible with asymtab_deserialize(sds).
@note The current format is `PLATFORM\0\n`, followed by a string of asymbol_serialize(asymbol_t*) outputs with no extra separators.

@return a @dynamicstring
**/
ASYMTAB_API sds asymtab_serialize(asymtab_t* symtab);

/**
@brief Deserialize a string into a symbol table object.
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.

The input is expected to have previously been created by asymtab_serialize(asymtab_t*).

@return a pointer to an asymtab_t (`err` will be set to `ASYMTAB_OK`)
@return `NULL` when parsing fails (`err` will be set to the appropriate error code)
**/
ASYMTAB_API asymtab_t* asymtab_deserialize(sds str, asymtab_error_t* err);

/**
@brief Deserialize a file into a symbol table object.
@param err Pointer to an error enum variable for error handling. Can be `NULL`, in which case distinction between error reasons will be impossible.

The contents of the file are expected to have previously been created by asymtab_serialize(asymtab_t*).

@return a pointer to an asymtab_t (`err` will be set to `ASYMTAB_OK`)
@return `NULL` when parsing fails (`err` will be set to the appropriate error code)
**/
ASYMTAB_API asymtab_t* asymtab_read(char* path, asymtab_error_t* err);

/**
@brief Remove a symbol from the symbol table, shifting the array to the left by one if needed.

@note This doesn't free the symbol object - you're expected to do it.
@note See asymtab_remove_free(asymtab_t*, asymbol_t*).

@return true if the symbol existed
@return false if it didn't.
**/
ASYMTAB_API bool asymtab_remove(asymtab_t* symtab, asymbol_t* sym);

/**
@brief Removes and frees a symbol from the symbol table, shifting the array to the left if needed.

@note The symbol object is unusable after calling this function.

@return true if the symbol existed
@return false if it didn't.
**/
ASYMTAB_API bool asymtab_remove_free(asymtab_t* symtab, asymbol_t* sym);

#endif//H_ASYMTAB
